<?php
    use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Primer ejemplo de Yii.';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Felicidades</h1>

        <p class="lead">Desarrollado Ramon Abramo</p>
    
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>API Yii</h2>

                <p>Todas las clases de Yii2</p>

                <p><a class="btn btn-default" href="https://www.yiiframework.com/doc/api/2.0">Api Yii2</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Manual Yii 2</h2>

                <p>Manual de Clases de Yii2</p>

                <p><a class="btn btn-default" href="https://www.yiiframework.com/doc/guide/2.0/es/">Guia de Yii2</a></p>
                <p><?= Html::a('Guia de Yii2', 'https://www.yiiframework.com/doc/guide/2.0/es/', ['class' => 'btn btn-default']) ?></p>
            </div>
        </div>

    </div>
</div>
